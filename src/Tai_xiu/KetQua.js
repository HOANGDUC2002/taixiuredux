import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME } from "./redux/constant/xucXacConstant";

class KetQua extends Component {
  render() {
    return (
      <div className="text-center pt-5 dipslay-4">
        <button onClick={this.props.handlePlayGame} className="btn btn-success">
          <span className="display-4">Play Game</span>
        </button>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(KetQua);
