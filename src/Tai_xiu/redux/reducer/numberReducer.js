import { PLAY_GAME } from "../constant/xucXacConstant";

let initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luachon: null,
};
export const xucXacReducer = (state = initialState, { type }) => {
  switch (type) {
    case PLAY_GAME: {
      // tạo mảng xúc xắc mới
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;
        return {
          img: `./imgXucSac/${random}.png`,
        };
      });
      state.mangXucXac = newMangXucXac;
      return { ...state };
    }
    default:
      return state;
  }
};
