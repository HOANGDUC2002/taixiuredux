// rcredux

import React, { Component } from "react";
import { connect } from "react-redux";
import { TAI, XIU } from "./redux/constant/xucXacConstant";

export class XucXac extends Component {
  state = {
    active: null,
  };
  handleChangeActive = (value) => {
    this.setState({ active: value });
  };
  renderListXucXac = () => {
    return this.props.mangXucXac.map((item, index) => {
      return (
        <img
          src={item.img}
          key={index}
          style={{ width: 100, margin: 10, borderRadius: 8 }}
          alt=""
        />
      );
    });
  };
  render() {
    let { active } = this.state;
    return (
      <div className="d-flex justify-content-between container pt-5">
        <button
          style={{
            width: 150,
            height: 150,
            fontSize: 40,
            transform: `scale(${active == TAI ? 1 : 0.5})`,
          }}
          onClick={() => {
            this.handleChangeActive(TAI);
          }}
          className="btn btn-danger"
        >
          Tài
        </button>
        <div>{this.renderListXucXac()}</div>
        <button
          style={{
            width: 150,
            height: 150,
            fontSize: 40,
            transform: `scale(${active == XIU ? 1 : 0.5})`,
          }}
          onClick={() => {
            this.handleChangeActive(XIU);
          }}
          className="btn btn-dark"
        >
          Xỉu
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  mangXucXac: state.xucXacReducer.mangXucXac,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
